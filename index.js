/*
	MongoDB Aggregation

	Syntax:
	db.collections.aggregate( 
		[
			{stage1},
			{stage2},
			{stage3}
		] 
	)
*/

db.fruits.insertMany([
		{
			"name": "Apple",
			"color": "Red",
			"stock": 20,
			"price": 40,
			"onSale": true,
			"origin": ["Philippines", "US"]
		},
		{
			"name": "Banana",
			"color": "Yellow",
			"stock": 15,
			"price": 20,
			"onSale": true,
			"origin": ["Philippines", "Ecuador"]
		},
		{
			"name": "Kiwi",
			"color": "Green",
			"stock": 25,
			"price": 50,
			"onSale": true,
			"origin": ["US", "China"]
		}, 
		{
			"name": "Mango",
			"color": "Yellow",
			"stock": 10,
			"price": 120,
			"onSale": false,
			"origin": ["Philippines", "India"]
		}
	]);

// Pipeline Stages
	
	//$match
		//filters the documents to pass only the documents that match tje specified condition/s to the next pipeline stage

		//syntax: { $match: { field:value }}

	db.fruits.aggregate(
		[
			{$match:{onSale:true}}
		]
	)

	/*$group
		//used to group elements together and field-value pairs using the data from the grouped elements

		Syntax:
		//{$group: {
				_id: "<expression",
				totalStock: {$sum: <expression>}
			}	
		}
	*/

	//group documents according to id, get the total of stocks. assign these to property name _id and totalStocks
	db.fruits.aggregate(
		[
			{$match:{onSale:true}},
			{$group:{_id: "$_id",
				totalStocks:{$sum: "$stock"}}}
		]
	)


	//$project
		//reshapes each document in the stream, such as including or excluding fields

		//syntax: { $project: { field: 1/0 } }

	//look for documents that has stock greate than 10, return found documents without id
		db.fruits.aggregate([
			{ $match: { "stock": {$gt:10} } },
			{ $project: { "_id":0 }
			//{ $project: { "_id":0, "name":1, "stock":1 }
				//name and stock fields only
		])

	//$sort
		//reorders the document stream by a specified sort key. 

		//syntax: { $sort {field: 1/-1}}

		db.fruits.aggregate([
			{ $match: { "stock": {$gt:10} } },
			{ $project: { "_id":0 },
			{ $sort: {"stock":1}}
		]}

	// $count
		//returns a count of the number of documents at this stage of the aggregation pipeline.

		// syntax: { $count: <stringName> }

	//look for documents that has a price of greater than or equal to 50, return the count of the number of documents and assign to property name fruitCount
	db.fruits.aggregate([
		{ $match: { "price": {$gte:50} } },
		{ $count: "fruitCount"}
	])


	// $unwind
		//Deconstructs an array field from the input documents to output a document for each element. Each output document replaces the array with an element value. For each input document, outputs n documents where n is the number of array elements and can be zero for an empty array.

		//syntax: { $unwind: <field: path> }

	// deconstruct the origin field of all documents and return those countries from 
	db.fruits.aggregate([
	{
		{ $unwind: "$origin"}
	}])

	//2nd stage, group documents accdg to their origin and count each document to 1 using sum operator and assign it to property name kinds
	db.fruits.aggregate([
		{$unwind: "$origin"}, 
		{$sort: {"$origin":1}},
		{$group: {_id: "$origin", Kinds: {$sum: 1} }}
		//auto assign value of 1 to count as dummy
	])

	//add sort
		db.fruits.aggregate([
			{$unwind: "$origin"}, 
	                {$group: {_id: "$origin", Kinds: {$sum: 1} }},
	                {$sort: {_id:1}}
		])


	